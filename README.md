# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

This Repository (Collection of code/scripts) is to help to get aware of Git at CalforFinance.
Folks should write some code/ideas in this folder and push it to repo. Do not create any private stuff here.

### How do I get set up? ###

* Create any file (text/python/cpp/java/jsp(For Frank))
* git add `FileName that you have created`  --> Say you have created frank.jsp , it should be git add frank.jsp
* git commit "Added a new File" --> I have provided random message, but message should reflect what is this file/changes is for
* git push  --> To push it to repo on cloud
* git log --pretty=oneline helps you see what all have been committed with their commit IDs

### Contribution guidelines ###

* Writing Sample Files
* Code review
* Try pushing it

### Who do I talk to? ###

* Shaik Asifullah, Akaki, Frank
* Sulkhan Metreveli
